﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace MoneyManagmentAPI.Filters
{
    public class MesudAuthentication : Attribute, IAuthenticationFilter
    {
        private CustomUserRole userRole;

        public MesudAuthentication(CustomUserRole role)
        {
            userRole = role;
        }

        private const string JWT_SECRET = "nc/k+MR6vm7fDAS9NTnoRehJH238WpPzArQMFXnQ3HW8d0Ys1Y+X4Q==";

        public bool AllowMultiple => throw new NotImplementedException();

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            AuthenticationHeaderValue authorization = context.Request.Headers.Authorization;
            string token = authorization.Parameter;

            SecurityToken validatedToken;
            ClaimsPrincipal claimsPrincipal = null;

            try
            {
                JwtSecurityTokenHandler jwt = new JwtSecurityTokenHandler();
                claimsPrincipal = jwt.ValidateToken(token, GetValidationParameters(JWT_SECRET), out validatedToken);
            }
            catch (Exception e)
            {
                context.ErrorResult = (IHttpActionResult) new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }

            string email = claimsPrincipal.Claims.FirstOrDefault().Value;
            string roleTemp = claimsPrincipal.Claims.Where(x => x.Properties.FirstOrDefault().Value == "role").FirstOrDefault().Value;
            CustomUserRole role = (CustomUserRole) Enum.Parse(typeof(CustomUserRole), roleTemp);

            if (role == userRole)
            {
                IIdentity identity = new GenericIdentity(email);
                IPrincipal principal = new GenericPrincipal(identity, new string[] { });
                context.Principal = principal;
            }
            else
                throw new Exception("Unauthorized.");
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult(0);
        }

        private TokenValidationParameters GetValidationParameters(string key)
        {
            return new TokenValidationParameters()
            {
                ValidateLifetime = false, // Because there is no expiration in the generated token
                ValidateAudience = false, // Because there is no audiance in the generated token
                ValidateIssuer = false,   // Because there is no issuer in the generated token
                IssuerSigningKey = new SymmetricSecurityKey(Convert.FromBase64String(key)) // The same key as the one that generate the token
            };
        }
    }

    public enum CustomUserRole
    {
        Admin,
        User
    }
}
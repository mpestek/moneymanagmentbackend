﻿using MoneyManagmentAPI.Helper;
using MoneyManagmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Web.Routing;

namespace MoneyManagmentAPI.Controllers
{
    [Authorize(Roles = "User")]
    [IsOwnerOfAccount]
    public class SavingCardsController : ApiController
    {
        SavingCardsDatabaseHelper databaseHelper;

        public HttpResponseMessage Get()
        {
            try
            {
                InitializeSavingCardsDatabaseHelper();
                IEnumerable<SavingCard> savingCards = databaseHelper.GetSavingCards();

                return Request.CreateResponse(HttpStatusCode.OK, savingCards);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, e.Message);
            }
        }

        public HttpResponseMessage Get(int id)
        {
            try
            {
                InitializeSavingCardsDatabaseHelper();
                SavingCard savingCard = databaseHelper.GetSavingCard(id);

                return Request.CreateResponse(HttpStatusCode.OK, savingCard);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, e.Message);
            }
        }

        public void Post(SavingCard savingCard)
        {
            InitializeSavingCardsDatabaseHelper();
            databaseHelper.CreateSavingCard(savingCard);
        }

        public void Put(SavingCardPOCO savingCard)
        {
            InitializeSavingCardsDatabaseHelper();
            databaseHelper.UpdateSavingCard(savingCard);
        }

        public void Delete(int id)
        {
            InitializeSavingCardsDatabaseHelper();
            databaseHelper.DeleteSavingCard(id);
        }

        private void InitializeSavingCardsDatabaseHelper()
        {
            databaseHelper = new SavingCardsDatabaseHelper(GetAccountIdFromRoute(Request));
        }

        private static string GetAuthorizedUserUsername()
        {
            IPrincipal principal = Thread.CurrentPrincipal;
            string username = principal.Identity.Name;

            return username;
        }

        private static int GetAccountIdFromRoute(HttpRequestMessage request)
        {
            IHttpRouteData routeData = request.GetRouteData();
            string accountId = (string)routeData.Values["accountId"];

            return int.Parse(accountId);
        }
    }
}
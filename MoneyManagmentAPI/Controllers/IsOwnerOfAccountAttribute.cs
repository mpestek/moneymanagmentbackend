﻿using MoneyManagmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace MoneyManagmentAPI.Controllers
{
    public class IsOwnerOfAccountAttribute : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple => throw new NotImplementedException();

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            try
            {
                string username = context.Principal.Identity.Name;
                int accountId = int.Parse((string)context.Request.GetRouteData().Values["accountId"]);

                bool userOwnsAccount = false;

                using (var db = new ApplicationDbContext())
                    userOwnsAccount = db.Users.SingleOrDefault(x => x.UserName == username).Accounts.Any(x => x.AccountID == accountId);

                if (!userOwnsAccount)
                    context.Principal = null;
            }
            catch (Exception)
            {
                context.Principal = null;
            }
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Http;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.IdentityModel.Tokens;
using MoneyManagmentAPI.Filters;
using MoneyManagmentAPI.Helper;
using MoneyManagmentAPI.Models;
using MoneyManagmentApp.Models;


namespace MoneyManagmentAPI.Controllers
{
    [Authorize(Roles="User")]
    public class SavingAccountsController : ApiController
    {
        private SavingAccountsDatabaseHelper databaseHelper;

        public HttpResponseMessage Get()
        {
            try
            {
                InitializeSavingAccountsDatabaseHelper();
                IEnumerable<Account> userAccounts = databaseHelper.GetUserSavingAccounts();

                return Request.CreateResponse(HttpStatusCode.OK, userAccounts);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, e.Message);
            }            
        }

        public HttpResponseMessage Get(int id)
        {
            try
            {
                InitializeSavingAccountsDatabaseHelper();
                Account account = databaseHelper.GetUserSavingAccount(id);

                return Request.CreateResponse(HttpStatusCode.OK, account);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, e.Message);
            }
        }

        public HttpResponseMessage Get(string accountName)
        {
            try
            {
                InitializeSavingAccountsDatabaseHelper();
                Account account = databaseHelper.GetUserSavingAccount(accountName);

                return Request.CreateResponse(HttpStatusCode.OK, account);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, e.Message);
            }
        }

        public void Post([FromBody]AccountPOCO account)
        {
            InitializeSavingAccountsDatabaseHelper();
            databaseHelper.CreateUserSavingAccount(account);
        }

        public void Put([FromBody]AccountPOCO account)
        {
            InitializeSavingAccountsDatabaseHelper();
            databaseHelper.UpdateUserSavingAccount(account);
        }

        public void Delete(int id)
        {
            InitializeSavingAccountsDatabaseHelper();
            databaseHelper.DeleteUserSavingAccount(id);
        }
        
        private void InitializeSavingAccountsDatabaseHelper()
        {
            databaseHelper = new SavingAccountsDatabaseHelper(GetAuthorizedUserUsername());
        }

        private static string GetAuthorizedUserUsername()
        {
            IPrincipal principal = Thread.CurrentPrincipal;
            string username = principal.Identity.Name;

            return username;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MoneyManagmentAPI.Models;
using MoneyManagmentApp.Models;

namespace MoneyManagmentAPI.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    public class UsersController : ApiController
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET api/<controller>
        public IEnumerable<UserRepresentation> Get()
        {
            return GetAllUserAccounts();
        }

        // GET api/<controller>/5
        public UserRepresentation Get(string username)
        {
           return GetUserAccount(username);
        }

        private UserRepresentation GetUserAccount(string username)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                ApplicationUser user = db.Users.Where(x => x.UserName == username).FirstOrDefault();
                
                if (user == null)
                    throw new ArgumentException("User with given username does not exist.");

                return new UserRepresentation(user);
            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        private IEnumerable<UserRepresentation> GetAllUserAccounts()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
                return GetUsersRepresentation(db.Users.Include(x => x.Roles).ToList());
        }

        private IEnumerable<UserRepresentation> GetUsersRepresentation(List<ApplicationUser> users)
        {
            return users.Select(x => new UserRepresentation(x));
        }
    }

    public class UserRepresentation
    {
        public string Username { get; set; }
        public List<string> Roles { get; set; }

        public UserRepresentation(ApplicationUser applicationUser)
        {
            Username = applicationUser.UserName;
            Roles = ConvertRolesIdsIntoRoleNames(applicationUser.Roles.Select(x => x.RoleId).ToList());
        }

        private List<string> ConvertRolesIdsIntoRoleNames(List<string> roleIds)
        {
            return roleIds.Select(x => ConvertRoleIdIntoRoleName(x)).ToList();
        }

        private string ConvertRoleIdIntoRoleName(string roleId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
                return db.Roles.Where(x => x.Id == roleId).FirstOrDefault().Name;
        }
    }
}
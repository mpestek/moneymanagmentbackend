﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MoneyManagmentAPI.Models;
using MoneyManagmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MoneyManagmentAPI.Controllers.Admin
{
    public class RolesController : ApiController
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // POST api/<controller>
        public async Task<HttpResponseMessage> PostAsync([FromBody]UserRoleRepresentation userRole)
        {
            try
            {
                string[] roles;

                using (ApplicationDbContext db = new ApplicationDbContext())
                    roles = db.Roles.Select(x => x.Name).ToArray();

                ApplicationUser user = await UserManager.FindByEmailAsync(userRole.Username);
                IdentityResult result = await UserManager.RemoveFromRolesAsync(user.Id, roles);
                result = await UserManager.AddToRoleAsync(user.Id, userRole.Role);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(
                    HttpStatusCode.NotFound,
                    new Exception("Either user was not found, or the role does not exist."));
            }
        }
    }

    public class UserRoleRepresentation
    {
        public string Username { get; set; }
        public string Role { get; set; }
    }
}
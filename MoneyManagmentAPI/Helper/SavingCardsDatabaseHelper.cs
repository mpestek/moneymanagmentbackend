﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MoneyManagmentApp.Models;

namespace MoneyManagmentAPI.Helper
{
    public class SavingCardsDatabaseHelper
    {
        private int accountId;

        public SavingCardsDatabaseHelper(int accountId)
        {
            this.accountId = accountId;
        }

        public IEnumerable<SavingCard> GetSavingCards()
        {
            List<SavingCard> savingCards = null;
            
            using (var db = new ApplicationDbContext())
                savingCards = db.SavingCards.Where(x => x.AccountID == accountId).ToList();

            return savingCards;
        }

        public SavingCard GetSavingCard(int id)
        {
            SavingCard savingCard = null;

            using (var db = new ApplicationDbContext())
                savingCard = db.SavingCards.SingleOrDefault(x => x.SavingCardID == id);

            return savingCard;
        }

        public void CreateSavingCard(SavingCard savingCard)
        {
            using (var db = new ApplicationDbContext())
            {
                Account account = db.Accounts.SingleOrDefault(x => x.AccountID == accountId);

                if (account.SavingCards == null)
                    account.SavingCards = new List<SavingCard>();

                account.SavingCards.Add(savingCard);

                db.SaveChanges();
            }
        }

        public void UpdateSavingCard(SavingCardPOCO updatedSavingCard)
        {
            using (var db = new ApplicationDbContext())
            {
                Account account = db.Accounts.Include(x => x.SavingCards).FirstOrDefault(x => x.AccountID == updatedSavingCard.AccountID);
                SavingCard outdatedSavingCard = account.SavingCards.FirstOrDefault(x => x.SavingCardID == updatedSavingCard.SavingCardID);
                decimal absDifferenceInBalance = Math.Abs(updatedSavingCard.Balance - outdatedSavingCard.Balance);

                if (updatedSavingCard.Balance > outdatedSavingCard.Balance)
                    account.DepositToSavingCard(outdatedSavingCard, absDifferenceInBalance);
                else
                    account.WithdrawFromSavingCard(outdatedSavingCard, absDifferenceInBalance);

                db.SaveChanges();
            }
        }

        public void DeleteSavingCard(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                SavingCard savingCardForRemoval = db.SavingCards.SingleOrDefault(x => x.SavingCardID == id);
                
                if (savingCardForRemoval != null)
                {
                    db.SavingCards.Remove(savingCardForRemoval);
                    db.SaveChanges();
                }
            }
        }
    }
}
﻿using MoneyManagmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;

namespace MoneyManagmentAPI.Helper
{
    public class SavingAccountsDatabaseHelper
    {
        private string username;

        public SavingAccountsDatabaseHelper(string username)
        {
            if (username == null)
                throw new ArgumentException("Username cannot be null.");

            this.username = username;
        }

        public string FindUserId()
        {
            string id = null;

            using (var db = new ApplicationDbContext())
                id = db.Users.Where(user => user.UserName == username).FirstOrDefault().Id;

            if (id == null)
                throw new ArgumentException("Username does not exist.");

            return id;
        }

        public IEnumerable<Account> GetUserSavingAccounts()
        {
            List<Account> userAccounts = null;

            using (var db = new ApplicationDbContext())
                userAccounts = GetUser(db).Accounts.ToList();
            
            if (userAccounts == null)
                throw new Exception("User does not have any accounts.");

            return userAccounts;
        }
        public Account GetUserSavingAccount(int accountId)
        {
            Account account = null;

            using (var db = new ApplicationDbContext())
            {
                ApplicationUser user = GetUser(db);
                account = user.Accounts.Where(x => x.AccountID == accountId).FirstOrDefault();
            }

            if (account == null)
                throw new Exception("User does not have an account with given id.");

            return account;
        }
        public Account GetUserSavingAccount(string accountName)
        {
            Account account = null;

            using (var db = new ApplicationDbContext())
            {
                ApplicationUser user = GetUser(db);
                account = user.Accounts.Where(x => x.Name == accountName).FirstOrDefault();
            }

            if (account == null)
                throw new ArgumentException("User does not have an account with that name.");

            return account;
        }
        public void CreateUserSavingAccount(AccountPOCO accountPOCO)
        {
            Account account = new Account(accountPOCO);

            using (var db = new ApplicationDbContext())
            {
                ApplicationUser user = GetUser(db);
                user.Accounts.Add(account);
                db.SaveChanges();
            }
        }
        public void UpdateUserSavingAccount(string accountName, AccountPOCO updatedAccountPOCO)
        {
            Account updatedAccount = new Account(updatedAccountPOCO);

            using (var db = new ApplicationDbContext())
            {
                ApplicationUser user = GetUser(db);
                Account accountPendingForUpdate = user.Accounts.Where(x => x.Name == accountName).FirstOrDefault();
                accountPendingForUpdate = updatedAccount;

                db.SaveChanges();
            }
        }
        public void DeleteUserSavingAccount(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                ApplicationUser user = GetUser(db);
                Account accountForRemoval = user.Accounts.SingleOrDefault(x => x.AccountID == id);

                if (accountForRemoval != null)
                {
                    user.Accounts.Remove(accountForRemoval);
                    db.SaveChanges();
                }
            }
        }
        public void DeleteUserSavingAccount(string accountName)
        {
            using (var db = new ApplicationDbContext())
            {
                ApplicationUser user = GetUser(db);
                Account accountForRemoval = user.Accounts.SingleOrDefault(x => x.Name == accountName);

                if (accountForRemoval != null)
                    user.Accounts.Remove(accountForRemoval);

                db.SaveChanges();
            }
        }
        public void UpdateUserSavingAccount(int accountId, Account updatedAccount)
        {
            using (var db = new ApplicationDbContext())
            {
                ApplicationUser user = GetUser(db);
                Account accountPendingForUpdate = user.Accounts.Where(x => x.AccountID == accountId).FirstOrDefault();
                accountPendingForUpdate = updatedAccount;

                db.SaveChanges();
            }
        }
        public void UpdateUserSavingAccount(AccountPOCO accountPOCO)
        {
            Account account = new Account(accountPOCO);

            using (var db = new ApplicationDbContext())
            {
                Account outdatedAccount = db.Accounts.FirstOrDefault(x => x.AccountID == account.AccountID);
                if (outdatedAccount == null)
                    throw new Exception("Account can't be updated if it doesn't exist!");

                AccountUpdater accountUpdater = new AccountUpdater(outdatedAccount);
                Account updatedAccount = accountUpdater.UpdateAccount(account);

                db.Entry(updatedAccount).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }
            
        private ApplicationUser GetUser(ApplicationDbContext db)
        {
            return db.Users
                .Where(x => x.UserName == username)
                .Include(x => x.Accounts.Select(y => y.SavingCards))
                .FirstOrDefault();
        }
    }
}
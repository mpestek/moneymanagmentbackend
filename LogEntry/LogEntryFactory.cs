﻿using MoneyManagmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging.LogEntry
{
    public static class LogEntryFactory
    {
        public static ILogEntry Create(LogEntryType logEntryType,
                                       Account account,
                                       decimal amount)
        {
            if (logEntryType == LogEntryType.Withdrawal)
                return new WithdrawalLogEntry(account, amount);
            else if (logEntryType == LogEntryType.Deposit)
                return new DepositLogEntry(account, amount);
            else if (logEntryType == LogEntryType.Transaction)
                throw new NotImplementedException();

            throw new NotImplementedException();
        }
    }
}

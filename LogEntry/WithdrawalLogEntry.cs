﻿using MoneyManagmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging.LogEntry
{
    public class WithdrawalLogEntry : ILogEntry
    {
        private Account account;
        private decimal amount;

        public WithdrawalLogEntry(Account account, decimal amount)
        {
            this.account = account;
            this.amount = amount;
        }

        public string CreateTextualRepresentation()
        {
            return String.Format("[Withdrawal] Account: {0}, Id: {1}, Amount: {2}, Balance: {3}.",
                account.Name, account.AccountID, amount, account.Balance);
        }
    }
}

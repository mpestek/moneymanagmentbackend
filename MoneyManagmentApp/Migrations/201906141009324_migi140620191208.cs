namespace MoneyManagmentApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migi140620191208 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SavingCards", "Account_AccountID", "dbo.Accounts");
            DropIndex("dbo.SavingCards", new[] { "Account_AccountID" });
            RenameColumn(table: "dbo.SavingCards", name: "Account_AccountID", newName: "AccountID");
            AlterColumn("dbo.SavingCards", "AccountID", c => c.Int(nullable: false));
            CreateIndex("dbo.SavingCards", "AccountID");
            AddForeignKey("dbo.SavingCards", "AccountID", "dbo.Accounts", "AccountID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SavingCards", "AccountID", "dbo.Accounts");
            DropIndex("dbo.SavingCards", new[] { "AccountID" });
            AlterColumn("dbo.SavingCards", "AccountID", c => c.Int());
            RenameColumn(table: "dbo.SavingCards", name: "AccountID", newName: "Account_AccountID");
            CreateIndex("dbo.SavingCards", "Account_AccountID");
            AddForeignKey("dbo.SavingCards", "Account_AccountID", "dbo.Accounts", "AccountID");
        }
    }
}

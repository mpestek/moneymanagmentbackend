namespace MoneyManagmentApp.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MoneyManagmentApp.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MoneyManagmentApp.Models.ApplicationDbContext context)
        {
            context.Roles.AddOrUpdate(new IdentityRole("User"));
            context.Roles.AddOrUpdate(new IdentityRole("Admin"));
        }
    }
}

﻿using MoneyManagmentApp;
using Logging.LogEntry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logging;
using Logging.LogEntry.LogInfo;

namespace MoneyManagmentApp.Models
{
    public class AccountManager
    {
        private Account account;
        private ILogger logger;

        public AccountManager(Account account, ILogger logger)
        {
            this.account = account;
            this.logger = logger;
        }

        public void Deposit(decimal amount)
        {
            try
            {
                account.Deposit(amount);
                logger.Log(LogEntryFactory.Create(AccountLogEntryType.Deposit, GetBasicTransactionInfo(amount)));
            }
            catch (Exception e)
            {
                logger.Log("Error " + e.Message);
                throw e;
            }
        }
        public void Withdraw(decimal amount)
        {
            try
            {
                account.Withdraw(amount);
                logger.Log(LogEntryFactory.Create(AccountLogEntryType.Withdrawal, GetBasicTransactionInfo(amount)));
            }
            catch (Exception e)
            {
                logger.Log(GetTimedExceptionMessage(e));
                throw e;
            }
        }

        public void DepositToSavingCard(SavingCard savingCard, decimal amount)
        {
            try
            {
                account.DepositToSavingCard(savingCard, amount);
                logger.Log(LogEntryFactory.Create(SavingCardTransactionLogEntryType.DepositToSavingCard, GetBasicSavingCardTransactionInfo(savingCard, amount)));
            }
            catch (Exception e)
            {
                logger.Log(GetTimedExceptionMessage(e));
                throw e;
            }
        }
        public void WithdrawFromSavingCard(SavingCard savingCard, decimal amount)
        {
            try
            {
                account.WithdrawFromSavingCard(savingCard, amount);
                logger.Log(LogEntryFactory.Create(SavingCardTransactionLogEntryType.WithdrawFromSavingCard, GetBasicSavingCardTransactionInfo(savingCard, amount)));
            }
            catch (Exception e)
            {
                logger.Log(GetTimedExceptionMessage(e));
                throw e;
            }
        }
        public void ReleaseFundsAndRemoveSavingCard(SavingCard savingCard)
        {
            try
            {
                account.ReleaseFundsAndRemoveSavingCard(savingCard);
                logger.Log(LogEntryFactory.Create(SavingCardModificationLogEntryType.SavingCardRemoval, GetSavingCardModificationInfo(savingCard)));
            }
            catch (Exception e)
            {
                logger.Log(GetTimedExceptionMessage(e));
                throw e;
            }
        }

        private string GetTimedExceptionMessage(Exception e)
        {
            return String.Format("{0} - {1} | [Error]: {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), e.Message);
        }

        private BasicTransactionInfo GetBasicTransactionInfo(decimal amount)
        {
            return new BasicTransactionInfo(account.Name,
                                            account.AccountID,
                                            account.Balance,
                                            amount);
        }
        private SavingCardTransactionInfo GetBasicSavingCardTransactionInfo(SavingCard savingCard, decimal amount)
        {
            return new SavingCardTransactionInfo(account.Name,
                                                 account.AccountID,
                                                 savingCard.Name,
                                                 amount,
                                                 account.Balance,
                                                 savingCard.Balance);
        }
        private SavingCardModificationInfo GetSavingCardModificationInfo(SavingCard savingCard)
        {
            return new SavingCardModificationInfo(account.Name,
                                                  account.AccountID,
                                                  savingCard.Name,
                                                  savingCard.Balance);
        }
    }
}

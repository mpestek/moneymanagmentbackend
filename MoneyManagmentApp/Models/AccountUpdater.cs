﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagmentApp.Models
{
    public class AccountUpdater
    {
        private Account outdatedAccount;

        public AccountUpdater(Account outdatedAccount)
        {
            this.outdatedAccount = outdatedAccount;
        }

        public Account UpdateAccount(Account updatedAccount)
        {
            outdatedAccount.Name = updatedAccount.Name;

            decimal balanceDifference = updatedAccount.Balance - outdatedAccount.Balance;
            if (balanceDifference > 0)
                outdatedAccount.Deposit(Math.Abs(balanceDifference));
            else
                outdatedAccount.Withdraw(Math.Abs(balanceDifference));

            return outdatedAccount;
        }
    }
}

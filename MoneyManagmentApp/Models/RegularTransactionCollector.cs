﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Data.Entity;
using DailyTimer;

namespace MoneyManagmentApp.Models
{
    public class RegularTransactionCollector
    {
        private static RegularTransactionCollector regularTransactionCollector;

        private TimeOfDayTimer timer;

        private RegularTransactionCollector() { }

        public void Start()
        {
            SetUpAndStartTimer();
        }

        private void SetUpAndStartTimer()
        {
            TimeOfDay timeOfDay = new TimeOfDay(12, 0);
            timer = new TimeOfDayTimer(timeOfDay, CollectionProcess);
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }

        public static RegularTransactionCollector Create()
        {
            if (regularTransactionCollector == null)
                regularTransactionCollector = new RegularTransactionCollector();

            return regularTransactionCollector;
        }

        private void CollectionProcess()
        {
            ResetTransactionCollectedStatusIfNewMonth();
            Collect();
        }

        private void Collect()
        {
            using (var db = new ApplicationDbContext())
            {
                foreach (var account in db.Accounts.Include(x => x.RegularTransactions))
                    account.CollectRegularTransactions();

                db.SaveChanges();
            }
        }

        private void ResetTransactionCollectedStatusIfNewMonth()
        {
            if (DateTime.Now.Day == 1)
                ResetTransactionsCollectedStatus();
        }

        private static void ResetTransactionsCollectedStatus()
        {
            using (var db = new ApplicationDbContext())
            {
                foreach (var regularTransaction in db.RegularTransactions)
                    regularTransaction.CollectedForThisMonth = false;

                db.SaveChanges();
            }
        }
    }
}

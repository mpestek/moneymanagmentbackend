﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoneyManagmentApp.Models
{
    public class AccountPOCO
    {
        public int AccountID { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
        public string ApplicationUserId { get; set; }
    }
}
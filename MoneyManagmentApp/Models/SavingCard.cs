﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagmentApp.Models
{
    public class SavingCard
    {
        [Key]
        public int SavingCardID { get; private set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Balance { get; internal set; }

        public virtual Account Account { get; private set; }
        public int AccountID { get; set; }
        
        private SavingCard() { }

        public SavingCard(string name, string description, Account account)
        {
            Name = name;
            Description = description;
            Account = account;
        }

        public SavingCard(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}

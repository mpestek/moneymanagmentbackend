﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagmentApp.Models
{
    public class Account
    {
        [Key]
        public int AccountID { get; private set; }
        public string Name { get; set; }
        public decimal Balance { get; private set; }

        public string ApplicationUserId { get; set; }

        public decimal TotalMoney { get => CalculateTotalAccountFunds(); }

        private decimal CalculateTotalAccountFunds()
        {
            if (SavingCards != null)
                return Balance + SavingCards.Sum<SavingCard>(x => x.Balance);

            return Balance;
        }

        public virtual ICollection<SavingCard> SavingCards { get; set; }
        public virtual ICollection<RegularTransaction> RegularTransactions { get; set; }

        private Account() { }

        public Account(AccountPOCO account)
        {
            AccountID = account.AccountID;
            Name = account.Name;
            Balance = account.Balance;
            ApplicationUserId = account.ApplicationUserId;
        }

        public Account(int id, string name, decimal totalMoney,
                       ICollection<SavingCard> savingCards,
                       ICollection<RegularTransaction> regularTransactions)
        {
            Name = name;
            Balance = totalMoney;
            SavingCards = savingCards;
            RegularTransactions = regularTransactions;
        }

        public Account(string name, decimal totalMoney, 
                       ICollection<SavingCard> savingCards,
                       ICollection<RegularTransaction> regularTransactions)
        {
            Name = name;
            Balance = totalMoney;
            SavingCards = savingCards;
            RegularTransactions = regularTransactions;
        }

        public void CollectRegularTransactions()
        {
            foreach (var regularTransaction in RegularTransactions)
                CollectRegularTransaction(regularTransaction);
        }

        private void CollectRegularTransaction(RegularTransaction regularTransaction)
        {
            if (!PreconditionsForCollectionAreMet(regularTransaction))
                return;

            if (Balance + regularTransaction.Amount < 0)
                throw new ArgumentException("Regular expense cannot be withdrawn from account. Insufficient funds.");

            Balance += regularTransaction.Amount;
            regularTransaction.CollectedForThisMonth = true;
        }

        private bool PreconditionsForCollectionAreMet(RegularTransaction regularTransaction)
        {
            return RegularTransactionHasStarted(regularTransaction) &&
                   IsTimeToCollect(regularTransaction) &&
                   !regularTransaction.CollectedForThisMonth;
        }

        private bool IsTimeToCollect(RegularTransaction regularTransaction)
        {
            return regularTransaction.DayOfTransaction <= DateTime.Now.Day;
        }

        private static bool RegularTransactionHasStarted(RegularTransaction regularTransaction)
        {
            return DateTime.Compare(DateTime.Now, regularTransaction.StartingDate) >= 0;
        }

        public void Deposit(decimal amount)
        {
            Balance += amount;
        }
        public void Withdraw(decimal amount)
        {
            if (Balance < amount)
                throw new ArgumentException("Insufficient funds for withdrawal.");

            Balance -= amount;
        }

        public void DepositToSavingCard(SavingCard savingCard, decimal amount)
        {
            if (Balance < amount)
                throw new ArgumentException("Insufficient funds for deposit to a saving card.");


            savingCard.Balance += amount;
            Balance -= amount;
        }
        public void WithdrawFromSavingCard(SavingCard savingCard, decimal amount)
        {
            if (savingCard.Balance < amount)
                throw new ArgumentException("Insufficient funds for withdrawal.");

            savingCard.Balance -= amount;
            Balance += amount;
        }

        public void ReleaseFundsAndRemoveSavingCard(SavingCard savingCard)
        {
            SavingCards.Remove(savingCard);
            Balance += savingCard.Balance;
        }

        public void TransferFundsBetweenSavingCards(SavingCard sender, SavingCard recipient, decimal amount)
        {
            if (sender.Balance < amount)
                throw new ArgumentException("Insufficient funds for specified transaction.");

            sender.Balance -= amount;
            recipient.Balance += amount;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagmentApp.Models
{
    public class SavingCardPOCO
    {
        public int SavingCardID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Balance { get; set; }

        public int AccountID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagmentApp.Models
{
    public class RegularTransaction
    {
        [Key]
        public int RegularTransactionID { get; private set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartingDate { get; private set; }
        public int DayOfTransaction { get; private set; }
        public decimal Amount { get; private set; }

        public bool CollectedForThisMonth { get; set; } = false;

        public virtual Account Account { get; set; }

        private RegularTransaction() { }

        public RegularTransaction(string name, string description, DateTime startingDate, int dayOfTransaction, decimal amount)
        {
            Name = name;
            Description = description;
            StartingDate = startingDate;
            DayOfTransaction = dayOfTransaction;
            Amount = amount;
        }

        public RegularTransaction(string name, string description, DateTime startingDate, int dayOfTransaction, decimal amount, Account account)
        {
            Name = name;
            Description = description;
            StartingDate = startingDate;
            DayOfTransaction = dayOfTransaction;
            Amount = amount;
            Account = account;
        }
    }
}

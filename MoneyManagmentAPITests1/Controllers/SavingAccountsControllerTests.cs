﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoneyManagmentAPI.Controllers;
using MoneyManagmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagmentAPI.Controllers.Tests
{
    [TestClass()]
    public class SavingAccountsControllerTests
    {
        DatabaseHe

        [TestMethod()]
        public void GetAccountByName()
        {
            AddUserWithTwoAccounts();

            Account account = null;

            HttpResponseMessage response = savingAccountsController.Get("Acc1");

            Assert.IsTrue(account != null);
        }

        private static void AddUserWithTwoAccounts()
        {
            using (var db = new ApplicationDbContext())
            {
                db.Users.Add(new ApplicationUser() { UserName = "test@gmail.com", Email = "test@gmail.com" });
                ApplicationUser user = db.Users.Where(x => x.UserName == "test@gmail.com").FirstOrDefault();
                user.Accounts.Add(new Account("Acc1", 10000, null, null));
                user.Accounts.Add(new Account("Acc2", 10000, null, null));

                db.SaveChanges();
            }
        }
    }
}
﻿namespace Logging
{
    public interface ILogEntry
    {
        string CreateTextualRepresentation();
    }
}
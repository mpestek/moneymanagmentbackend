﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging.LogEntry.LogInfo
{
    public class BasicTransactionInfo
    {
        public BasicTransactionInfo(string accountName, int accountID, decimal accountBalance, decimal amountToBeTransfered)
        {
            AccountName = accountName;
            AccountID = accountID;
            AccountBalance = accountBalance;
            AmountToBeTransfered = amountToBeTransfered;
        }

        public string AccountName { get; set; }
        public int AccountID { get; set; }
        public decimal AccountBalance { get; set; }
        public decimal AmountToBeTransfered { get; set; }
    }
}

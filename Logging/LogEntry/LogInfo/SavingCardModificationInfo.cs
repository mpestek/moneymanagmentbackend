﻿namespace Logging.LogEntry.LogInfo
{
    public class SavingCardModificationInfo
    {
        public string AccountName { get; set; }
        public int AccountID { get; set; }
        public string SavingCardName { get; set; }
        public decimal ReleasedFunds { get; set; }

        public SavingCardModificationInfo(string accountName, int accountID, string savingCardName, decimal releasedFunds)
        {
            AccountName = accountName;
            AccountID = accountID;
            SavingCardName = savingCardName;
            ReleasedFunds = releasedFunds;
        }
    }
}
﻿namespace Logging.LogEntry.LogInfo
{
    public class SavingCardTransactionInfo
    {
        public SavingCardTransactionInfo(string accountName, int accountID, string savingCardName, decimal amountToBeTransfered, decimal accountBalance, decimal savingCardBalance)
        {
            AccountName = accountName;
            AccountID = accountID;
            SavingCardName = savingCardName;
            AmountToBeTransfered = amountToBeTransfered;
            AccountBalance = accountBalance;
            SavingCardBalance = savingCardBalance;
        }

        public string AccountName { get; set; }
        public int AccountID { get; set; }
        public string SavingCardName { get; set; }
        public decimal AmountToBeTransfered { get; set; }
        public decimal AccountBalance { get; set; }
        public decimal SavingCardBalance { get; set; }
    }
}
﻿using Logging.LogEntry.LogEntries;
using Logging.LogEntry.LogInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging.LogEntry
{
    public static class LogEntryFactory
    {
        public static ILogEntry Create(AccountLogEntryType AccountLogEntryType,
                                       BasicTransactionInfo transactionInfo)
        {
            if (AccountLogEntryType == AccountLogEntryType.Withdrawal)
                return new WithdrawalLogEntry(transactionInfo);
            else if (AccountLogEntryType == AccountLogEntryType.Deposit)
                return new DepositLogEntry(transactionInfo);
            else if (AccountLogEntryType == AccountLogEntryType.Transaction)
                throw new NotImplementedException();

            throw new NotImplementedException();
        }

        public static ILogEntry Create(SavingCardTransactionLogEntryType SavingCardTransactionLogEntryType,
                                       SavingCardTransactionInfo transactionInfo)
        {
            if (SavingCardTransactionLogEntryType == SavingCardTransactionLogEntryType.DepositToSavingCard)
                return new DepositToSavingCardLogEntry(transactionInfo);
            else if (SavingCardTransactionLogEntryType == SavingCardTransactionLogEntryType.WithdrawFromSavingCard)
                return new WithdrawalFromSavingCardLogEntry(transactionInfo);

            throw new NotImplementedException();
        }

        public static ILogEntry Create(SavingCardModificationLogEntryType SavingCardModificationLogEntryType,
                                       SavingCardModificationInfo modificationInfo)
        {
            if (SavingCardModificationLogEntryType == SavingCardModificationLogEntryType.SavingCardRemoval)
                return new RemoveSavingCardLogEntry(modificationInfo);

            throw new NotImplementedException();
        }
    }
}

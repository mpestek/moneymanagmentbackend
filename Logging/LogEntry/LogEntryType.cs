﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging.LogEntry
{
    public enum AccountLogEntryType
    {
		Withdrawal,
		Deposit,
		Transaction
    }

    public enum SavingCardModificationLogEntryType
    {
        SavingCardRemoval
    }

    public enum SavingCardTransactionLogEntryType
    {
        DepositToSavingCard,
        WithdrawFromSavingCard
    }
}

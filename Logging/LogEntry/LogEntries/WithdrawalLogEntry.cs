﻿using Logging.LogEntry.LogInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging.LogEntry.LogEntries
{
    public class WithdrawalLogEntry : ILogEntry
    {
        private BasicTransactionInfo transactionInfo;

        public WithdrawalLogEntry(BasicTransactionInfo transactionInfo)
        {
            this.transactionInfo = transactionInfo;
        }

        public string CreateTextualRepresentation()
        {
            return String.Format("{4} - {5} | [Withdrawal] Account: {0}, Id: {1}, Amount: {2}, Balance: {3}.",
                                 transactionInfo.AccountName, 
                                 transactionInfo.AccountID, 
                                 transactionInfo.AmountToBeTransfered, 
                                 transactionInfo.AccountBalance,
                                 DateTime.Now.ToShortDateString(),
                                 DateTime.Now.ToShortTimeString());
        }
    }
}

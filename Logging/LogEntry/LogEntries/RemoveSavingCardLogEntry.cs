﻿using Logging.LogEntry.LogInfo;
using System;

namespace Logging.LogEntry.LogEntries
{
    public class RemoveSavingCardLogEntry : ILogEntry
    {
        private SavingCardModificationInfo modificationInfo;

        public RemoveSavingCardLogEntry(SavingCardModificationInfo modificationInfo)
        {
            this.modificationInfo = modificationInfo;
        }

        public string CreateTextualRepresentation()
        {
            return String.Format("{4} - {5} | [SavingCard Removal] Account: {0}, AccountID: {1}, SavingCard: {2}, Released Funds: {3}.",
                                 modificationInfo.AccountName, 
                                 modificationInfo.AccountID, 
                                 modificationInfo.SavingCardName, 
                                 modificationInfo.ReleasedFunds,
                                 DateTime.Now.ToShortDateString(),
                                 DateTime.Now.ToShortTimeString());
        }
    }
}
﻿using Logging.LogEntry.LogInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging.LogEntry.LogEntries
{
    public class DepositToSavingCardLogEntry : ILogEntry
    {
        public DepositToSavingCardLogEntry(SavingCardTransactionInfo transactionInfo)
        {
            this.transactionInfo = transactionInfo;
        }

        private SavingCardTransactionInfo transactionInfo { get; set; }

        public string CreateTextualRepresentation()
        {
            return String.Format("{6} - {7} | [Deposit to SavingCard] Account: {0}, ID = {1}, SavingCard: {2}, Amount: {3}, Account Balance: {4}, SavingCard Balance: {5}",
                                  transactionInfo.AccountName, 
                                  transactionInfo.AccountID, 
                                  transactionInfo.SavingCardName,
                                  transactionInfo.AmountToBeTransfered, 
                                  transactionInfo.AccountBalance, 
                                  transactionInfo.SavingCardBalance,
                                  DateTime.Now.ToShortDateString(),
                                 DateTime.Now.ToShortTimeString());
        }
    }
}

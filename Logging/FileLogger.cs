﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace Logging
{
    public class FileLogger : ILogger
    {
        private string path = ConfigurationManager.AppSettings["LogFilePath"];

        public void Log(string entry)
        {
            AppendLineToFile(entry);
        }

        public void Log(ILogEntry logEntry)
        {
            AppendLineToFile(logEntry.CreateTextualRepresentation());
        }

        private void AppendLineToFile(string entry)
        {
            using (StreamWriter writer = new StreamWriter(path, true))
                writer.WriteLine(entry);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyTimer
{
    public class TimeOfDay
    {
        public int Hour { get; private set; }
        public int Minute { get; private set; }
        
        public TimeOfDay(int hour, int minute)
        {
            if (InvalidHour(hour) || InvalidMinute(minute))
                throw new ArgumentException("Invalid time set for time of day (Hour [0, 23], Minute [0, 59]). Example: 15:32.");

            Hour = hour;
            Minute = minute;
        }

        private bool InvalidMinute(int minute)
        {
            return (minute < 0 || minute > 59);
        }

        private bool InvalidHour(int hour)
        {
            return (hour < 0 || hour > 23);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace DailyTimer
{
    public class TimeOfDayTimer
    {
        public TimeOfDay TimeOfDay { get; private set; }
        public Action Callback { get; private set; }
        public double Interval { get; set; } = ONE_DAY; 

        private Timer timer = new Timer();
        private bool firstRun = true;
        private DateTime firingDate;

        private const double ONE_DAY = 24 * 60 * 60 * 1000;

        public TimeOfDayTimer(TimeOfDay timeOfDay, Action callback)
        {
            TimeOfDay = timeOfDay;
            Callback = callback;

            firingDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                      TimeOfDay.Hour, TimeOfDay.Minute, 0);
        }

        public void Start()
        {
            PrepareAndStartTimer(CalculateFirstInterval());
        }

        private double CalculateFirstInterval()
        {
            if (DateTime.Now > firingDate)
                return (firingDate.AddDays(1) - DateTime.Now).TotalMilliseconds;

            return (firingDate - DateTime.Now).TotalMilliseconds;
        }
        private void PrepareAndStartTimer(double interval)
        {
            timer = new Timer(interval);
            timer.Elapsed += (sender, e) => ExtendedTimerCallback();
            timer.AutoReset = false;
            timer.Enabled = true;
        }
        private void ExtendedTimerCallback()
        {
            if (firstRun)
            {
                firstRun = false;
                HandleFirstRun();
            }

            Callback();
        }
        private void HandleFirstRun()
        {
            timer.Enabled = false;

            timer.Interval = Interval;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        public void Stop()
        {
            timer.Dispose();
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoneyManagmentAPI.Helper;
using MoneyManagmentApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagmentAPI.Helper.Tests
{
    [TestClass()]
    public class SavingCardsDatabaseHelperTests
    {
        private string username = "test@gmail.com";
        private SavingCardsDatabaseHelper databaseHelper;

        [TestMethod()]
        public void GetSavingCardsTest()
        {
            InitializeTestingConditions();

            ICollection<SavingCard> savingCards = databaseHelper.GetSavingCards().ToList();

            Assert.IsTrue(savingCards.Count == 2);
        }

        [TestMethod()]
        public void GetSavingCardTest()
        {
            InitializeTestingConditions();

            SavingCard savingCard = databaseHelper.GetSavingCard(GetSavingCardId("Sc1"));

            Assert.IsTrue(savingCard != null);
        }

        [TestMethod()]
        public void CreateSavingCardTest()
        {
            InitializeTestingConditions();
            databaseHelper.CreateSavingCard(new SavingCard("TestSc1", "TestSc1Description"));

            SavingCard foundSavingCard = databaseHelper.GetSavingCard(GetSavingCardId("TestSc1"));

            Assert.IsTrue(foundSavingCard != null);
        }

        //[TestMethod()]
        //public void UpdateSavingCardTest()
        //{
        //    InitializeTestingConditions();
        //    SavingCard savingCard = databaseHelper.GetSavingCard(GetSavingCardId("Sc1"));
        //    savingCard.Name = "Hura";

        //    databaseHelper.UpdateSavingCard(savingCard);

        //    Assert.IsTrue(databaseHelper.GetSavingCard(savingCard.SavingCardID).Name == "Hura");
        //}

        [TestMethod()]
        public void DeleteSavingCardTest()
        {
            InitializeTestingConditions();

            int savingCardId = GetSavingCardId("Sc1");
            databaseHelper.DeleteSavingCard(savingCardId);

            Assert.IsTrue(databaseHelper.GetSavingCard(savingCardId) == null);
        }

        private void InitializeDatabaseHelper()
        {
            databaseHelper = new SavingCardsDatabaseHelper(GetAccountId("Acc1"));
        }

        private void InitializeTestingConditions()
        {
            AddUserWithTwoAccounts();
            AddSavingCardsToAccount();
            InitializeDatabaseHelper();
        }

        private void AddUserWithTwoAccounts()
        {
            using (var db = new ApplicationDbContext())
            {
                ApplicationUser user = new ApplicationUser() { UserName = username, Email = username };

                user.Accounts.Add(new Account("Acc1", 111, new List<SavingCard>(), null));
                user.Accounts.Add(new Account("Acc2", 222, new List<SavingCard>(), null));

                ApplicationUser applicationUser = db.Users.SingleOrDefault(x => x.UserName == user.UserName);
                if (applicationUser != null)
                {
                    foreach (Account account in applicationUser.Accounts)
                        if (account.SavingCards != null)
                            db.SavingCards.RemoveRange(account.SavingCards);

                    db.Accounts.RemoveRange(applicationUser.Accounts);
                    db.Users.Remove(applicationUser);
                    db.SaveChanges();
                }

                db.Users.Add(user);

                db.SaveChanges();
            }
        }

        private void AddSavingCardsToAccount()
        {
            using (var db = new ApplicationDbContext())
            {
                int accountId = GetAccountId("Acc1");
                
                List<SavingCard> savingCards = new List<SavingCard>();
                savingCards.Add(new SavingCard("Sc1", "This is Saving Card number 1."));
                savingCards.Add(new SavingCard("Sc2", "This is Saving Card number 2."));

                db.Accounts.SingleOrDefault(x => x.AccountID == accountId).SavingCards = savingCards;

                db.SaveChanges();
            }
        }

        private int GetAccountId(string accountName)
        {
            int accountId = -1;

            using (var db = new ApplicationDbContext())
                accountId = db.Users.SingleOrDefault(x => x.UserName == username).Accounts.SingleOrDefault(x => x.Name == accountName).AccountID;

            return accountId;
        }

        private int GetSavingCardId(string savingCardName)
        {
            int savingCardId = -1;

            using (var db = new ApplicationDbContext())
            {
                int accountId = GetAccountId("Acc1");
                Account account = db.Accounts.Include(x => x.SavingCards).SingleOrDefault(x => x.AccountID == accountId);
                savingCardId = account.SavingCards.SingleOrDefault(x => x.Name == savingCardName).SavingCardID;
            }

            return savingCardId;
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoneyManagmentAPI.Controllers;
using MoneyManagmentAPI.Helper;
using MoneyManagmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagmentAPI.Controllers.Tests
{
    [TestClass()]
    public class SavingAccountsControllerTests
    {
        private static string username = "test@gmail.com";
        private static SavingAccountsDatabaseHelper databaseHelper = new SavingAccountsDatabaseHelper(username);

        [TestMethod()]
        public void GetUserSavingAccountsTest()
        {
            AddUserWithTwoAccounts();

            ICollection<Account> accounts = databaseHelper.GetUserSavingAccounts().ToList();

            Assert.IsTrue(accounts.Count == 2);
        }

        [TestMethod()]
        public void GetUserSavingAccountTest()
        {
            AddUserWithTwoAccounts();

            Account account = null;
            account = databaseHelper.GetUserSavingAccount("Acc1");

            Assert.IsTrue(account != null);
            Assert.IsTrue(account.Balance == 111);
        }


        private void AddUserWithTwoAccounts()
        {
            using (var db = new ApplicationDbContext())
            {
                ApplicationUser user = new ApplicationUser() { UserName = "test@gmail.com", Email = "test@gmail.com" };

                user.Accounts.Add(new Account("Acc1", 111, null, null));
                user.Accounts.Add(new Account("Acc2", 222, null, null));

                ApplicationUser applicationUser = db.Users.SingleOrDefault(x => x.UserName == user.UserName);
                if (applicationUser != null)
                {
                    db.Accounts.RemoveRange(applicationUser.Accounts);
                    db.Users.Remove(applicationUser);
                    db.SaveChanges();
                }

                db.Users.Add(user);
                
                db.SaveChanges();
            }
        }

        //[TestMethod()]
        //public void CreateUserSavingAccountTest()
        //{
        //    AddUserWithTwoAccounts();

        //    databaseHelper.CreateUserSavingAccount(new Account("Acc3", 333, null, null));
        //    Account account = null;
        //    account = databaseHelper.GetUserSavingAccount("Acc3");

        //    Assert.IsTrue(account != null);
        //    Assert.IsTrue(account.Balance == 333);
        //}

        //[TestMethod()]
        //public void UpdateUserSavingAccountTest()
        //{
        //    AddUserWithTwoAccounts();

        //    Account updatedAccount = databaseHelper.GetUserSavingAccount("Acc1");
        //    updatedAccount.Name = "Acc3";
        //    databaseHelper.UpdateUserSavingAccount(updatedAccount);

        //    IEnumerable<Account> userSavingAccounts = databaseHelper.GetUserSavingAccounts();
        //    Assert.IsTrue(userSavingAccounts.All(x => x.Name != "Acc1"));
        //}

        [TestMethod()]
        public void DeleteUserSavingAccountTest()
        {
            try
            {
                AddUserWithTwoAccounts();

                databaseHelper.DeleteUserSavingAccount("Acc1");

                databaseHelper.GetUserSavingAccount("Acc1");
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.GetType() == typeof(ArgumentException));
                Assert.IsTrue(e.Message == "User does not have an account with that name.");
            }
        }
    }
}
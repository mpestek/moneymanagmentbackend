﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoneyManagmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Data.Entity;

namespace UnitTestMoneyManagment
{
    [TestClass]
    public class RegularTransactionCollectorTest
    {
        private Account testAccount;

        [TestMethod]
        public void RegularTransactionCollectionTest()
        {
            throw new NotImplementedException();

            RegularTransactionCollector regularTransactionCollector =
                RegularTransactionCollector.Create();

            InitializeTestAccount();
            InitializeRegularTransactions();
            SaveAccountToDB();

            decimal balanceBeforeCollection = testAccount.Balance;

            //regularTransactionCollector.Interval = 1000;
            regularTransactionCollector.Start();

            Assert.AreEqual(balanceBeforeCollection, getTestAccountFromDB().Balance);

            Thread.Sleep(1500);
            regularTransactionCollector.Stop();
            Assert.AreEqual(balanceBeforeCollection + 400, getTestAccountFromDB().Balance);
        }

        private Account getTestAccountFromDB()
        {
            using (var db = new ApplicationDbContext())
            {
                Account acc = db.Accounts.Where<Account>(x => x.Name == "RegularTransCollectorTestAccount")
                                         .Include(x => x.RegularTransactions)
                                         .FirstOrDefault<Account>();

                return acc;
            }
        }

        private void SaveAccountToDB()
        {
            using (var db = new ApplicationDbContext())
            {
                foreach (var regTrans in db.RegularTransactions)
                    db.RegularTransactions.Remove(regTrans);

                foreach (var acc in db.Accounts)
                    db.Accounts.Remove(acc);

                db.Accounts.Add(testAccount);
                db.SaveChanges();
            }
        }

        private void InitializeRegularTransactions()
        {
            testAccount.RegularTransactions.Add(
                new RegularTransaction("RegT1",
                                       "...",
                                       DateTime.Now,
                                       5,
                                       100));
            testAccount.RegularTransactions.Add(
                new RegularTransaction("RegT2",
                                       "...",
                                       DateTime.Now,
                                       6,
                                       200));
            testAccount.RegularTransactions.Add(
                new RegularTransaction("RegT3",
                                       "...",
                                       new DateTime(2019, 5, 4),
                                       5,
                                       300));
            testAccount.RegularTransactions.Add(
                new RegularTransaction("RegT4",
                                       "...",
                                       new DateTime(2019, 5, 6),
                                       5,
                                       400));
        }

        private void InitializeTestAccount()
        {
            testAccount = new Account("RegularTransCollectorTestAccount",
                                                  1000,
                                                  new List<SavingCard>(),
                                                  new List<RegularTransaction>());
        }
    }
}

﻿using Logging;
using Logging.LogEntry;
using Logging.LogEntry.LogInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoneyManagmentApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagmentApp.Models.Tests
{
    [TestClass()]
    public class AccountLoggerAdapterTests
    {
        private Account account;
        private SavingCard savingCard;
        private AccountManager accountManager;

        [TestMethod()]
        public void LogDepositToAccountTest()
        {
            account = new Account("MojAcc", 523, null, null);
            AccountManager accountLoggerAdapter = new AccountManager(account, new FileLogger());
            accountLoggerAdapter.Deposit(200);

            Assert.IsTrue(EntryExistsInLog(LogEntryFactory.Create(AccountLogEntryType.Deposit, GetBasicTransactionInfo(200)).CreateTextualRepresentation()));
        }

        [TestMethod()]
        public void LogWithdrawalFromAccountTest()
        {
            try
            {
                account = new Account("MojAcc", 523, null, null);
                AccountManager accountLoggerAdapter = new AccountManager(account, new FileLogger());
                accountLoggerAdapter.Withdraw(700);

                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.AreEqual(typeof(ArgumentException), e.GetType());
            }
        }

        [TestMethod()]
        public void LogDepositToSavingCardTest()
        {
            InitializeBasicTestingCase();
            AccountManager accountManager = new AccountManager(account, new FileLogger());

            accountManager.DepositToSavingCard(savingCard, 300);

            Assert.IsTrue(EntryExistsInLog(LogEntryFactory.Create(SavingCardTransactionLogEntryType.DepositToSavingCard, GetBasicSavingCardTransactionInfo(savingCard, 300)).CreateTextualRepresentation()));
        }

        [TestMethod()]
        public void LogSavingCardRemovalTest()
        {
            InitializeBasicTestingCase();
            AccountManager accountManager = new AccountManager(account, new FileLogger());

            accountManager.ReleaseFundsAndRemoveSavingCard(savingCard);

            Assert.IsTrue(EntryExistsInLog(LogEntryFactory.Create(SavingCardModificationLogEntryType.SavingCardRemoval, GetSavingCardModificationInfo(savingCard)).CreateTextualRepresentation()));
        }

        [TestMethod()]
        public void LogWithdrawalFromSavingCardTest()
        {
            InitializeBasicTestingCase();
            account.DepositToSavingCard(savingCard, 200);

            accountManager.WithdrawFromSavingCard(savingCard, 150);

            Assert.IsTrue(EntryExistsInLog(LogEntryFactory.Create(SavingCardTransactionLogEntryType.WithdrawFromSavingCard, GetBasicSavingCardTransactionInfo(savingCard, 150)).CreateTextualRepresentation()));
        }

        [TestMethod()]
        public void LogWithdrawTooMuchFromSavingCardTest()
        {
            try
            {
                InitializeBasicTestingCase();
                account.DepositToSavingCard(savingCard, 150);

                accountManager.WithdrawFromSavingCard(savingCard, 200);

                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.AreEqual(typeof(ArgumentException), e.GetType());
            }
        }

        private void InitializeBasicTestingCase()
        {
            account = new Account("MojAcc", 523, new List<SavingCard>(), null);
            savingCard = new SavingCard("SC1", "", null);
            account.SavingCards.Add(savingCard);
            accountManager = new AccountManager(account, new FileLogger());
        }

        private bool EntryExistsInLog(string entry)
        {
            string logPath = ConfigurationManager.AppSettings["LogFilePath"];

            string[] logContent = File.ReadAllLines(logPath);

            return logContent.Contains(entry);
        }

        private BasicTransactionInfo GetBasicTransactionInfo(decimal amount)
        {
            return new BasicTransactionInfo(account.Name,
                                            account.AccountID,
                                            account.Balance,
                                            amount);
        }
        private SavingCardTransactionInfo GetBasicSavingCardTransactionInfo(SavingCard savingCard, decimal amount)
        {
            return new SavingCardTransactionInfo(account.Name,
                                                 account.AccountID,
                                                 savingCard.Name,
                                                 amount,
                                                 account.Balance,
                                                 savingCard.Balance);
        }
        private SavingCardModificationInfo GetSavingCardModificationInfo(SavingCard savingCard)
        {
            return new SavingCardModificationInfo(account.Name,
                                                  account.AccountID,
                                                  savingCard.Name,
                                                  savingCard.Balance);
        }
    }
}
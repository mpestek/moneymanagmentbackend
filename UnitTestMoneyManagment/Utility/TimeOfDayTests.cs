﻿using DailyTimer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoneyManagmentApp.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagmentApp.Utility.Tests
{
    [TestClass()]
    public class TimeOfDayTests
    {
        TimeOfDay timeOfDay;

        [TestMethod()]
        public void TimeOfDayLegalCreationTest()
        {
            timeOfDay = new TimeOfDay(23, 59);

            Assert.AreEqual(timeOfDay.Hour, 23);
            Assert.AreEqual(timeOfDay.Minute, 59);
        }

        [TestMethod()]
        public void TimeOfDayLegalCreationSecondTest()
        {
            timeOfDay = new TimeOfDay(0, 0);

            Assert.AreEqual(timeOfDay.Hour, 0);
            Assert.AreEqual(timeOfDay.Minute, 0);
        }

        [TestMethod()]
        public void TimeOfDayLegalCreationThirdTest()
        {
            timeOfDay = new TimeOfDay(0, 20);

            Assert.AreEqual(timeOfDay.Hour, 0);
            Assert.AreEqual(timeOfDay.Minute, 20);
        }

        [TestMethod()]
        public void TimeOfDayLegalCreationFourthTest()
        {
            timeOfDay = new TimeOfDay(20, 0);

            Assert.AreEqual(timeOfDay.Hour, 20);
            Assert.AreEqual(timeOfDay.Minute, 0);
        }

        [TestMethod]
        public void TimeOfDayIllegalCreationTest()
        {
            try
            {
                timeOfDay = new TimeOfDay(-2, 0);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.GetType(), typeof(ArgumentException));
            }
        }

        [TestMethod]
        public void TimeOfDayIllegalCreationSecondTest()
        {
            try
            {
                timeOfDay = new TimeOfDay(24, 0);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.GetType(), typeof(ArgumentException));
            }
        }

        [TestMethod]
        public void TimeOfDayIllegalCreationThirdTest()
        {
            try
            {
                timeOfDay = new TimeOfDay(0, -1);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.GetType(), typeof(ArgumentException));
            }
        }

        [TestMethod]
        public void TimeOfDayIllegalCreationFourthTest()
        {
            try
            {
                timeOfDay = new TimeOfDay(0, 60);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.GetType(), typeof(ArgumentException));
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoneyManagmentApp.Models;
using System.Data.Entity;

namespace UnitTestMoneyManagment
{
    [TestClass]
    public class AccountTest
    {
        private Account account;
        private SavingCard savingCard;

        private List<SavingCard> savingCards = new List<SavingCard>();
        private List<RegularTransaction> regularTransactions = new List<RegularTransaction>();

        private decimal totalMoneyBeforeTransaction;
        private decimal balanceBeforeTransaction;

        private decimal savingCardBalanceBeforeTransaction;

        [TestMethod]
        public void CreateNewAccountTest()
        {
            InitializeAccount();

            Assert.AreEqual(account.Name, "RandomAccount");
        }

        private void InitializeAccount()
        {
            account = new Account("RandomAccount", 0, new List<SavingCard>(), new List<RegularTransaction>());
        }

        [TestMethod]
        public void AddSavingCardToAccountTest()
        {
            InitializeAccount();
            InitializeSavingCards();

            Assert.AreEqual(savingCard.Account.Name, account.Name);
        }

        [TestMethod]
        public void DepositToAccountTest()
        {
            InitializeStandardTestingConditions();
            decimal depositAmount = 100;
            
            account.Deposit(depositAmount);

            Assert.AreEqual(account.Balance, balanceBeforeTransaction + depositAmount);
        }

        [TestMethod]
        public void WithdrawFromAccountTest()
        {
            InitializeStandardTestingConditions();
            decimal amount = 100;
            
            account.Withdraw(amount);

            Assert.AreEqual(account.Balance + amount, balanceBeforeTransaction);
        }

        [TestMethod]
        public void WithdrawTooMuchFromAccountTest()
        {
            try
            {
                InitializeAccountWithBudget(90);
                decimal amount = 100;

                account.Withdraw(amount);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.GetType(), typeof(ArgumentException));
            }

            if (account.Balance < 0)
                Assert.Fail();
        }

        [TestMethod]
        public void DepositToSavingCardTest()
        {
            InitializeStandardTestingConditions();
            decimal depositAmount = 100;

            account.DepositToSavingCard(savingCard, depositAmount);

            Assert.AreEqual(balanceBeforeTransaction - depositAmount, account.Balance);
            Assert.AreEqual(savingCardBalanceBeforeTransaction + depositAmount, savingCard.Balance);
        }

        [TestMethod]
        public void DepositTooMuchToSavingCardTest()
        {
            try
            {
                InitializeAccountWithBudget(1000);
                InitializeSavingCards();
                InitializeBalancesBeforeTransaction();
                decimal depositAmount = 1500;

                account.DepositToSavingCard(savingCard, depositAmount);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.GetType(), typeof(ArgumentException));
            }

            if (account.Balance < 0)
                Assert.Fail();
        }

        [TestMethod]
        public void WithdrawFromSavingCardTest()
        {
            InitializeAccountWithBudget(9000);
            InitializeSavingCards();

            decimal amount = 100;
            account.DepositToSavingCard(savingCard, 2 * amount);

            InitializeBalancesBeforeTransaction();

            account.WithdrawFromSavingCard(savingCard, amount);

            Assert.AreEqual(savingCardBalanceBeforeTransaction - amount, savingCard.Balance);
            Assert.AreEqual(balanceBeforeTransaction + amount, account.Balance);
        }

        [TestMethod]
        public void WithdrawTooMuchFromSavingCardTest()
        {
            try
            {
                InitializeAccountWithBudget(9000);
                InitializeSavingCards();

                decimal amount = 100;
                account.DepositToSavingCard(savingCard, amount);

                InitializeBalancesBeforeTransaction();

                account.WithdrawFromSavingCard(savingCard, 2 * amount);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.GetType(), typeof(ArgumentException));
            }

            if (savingCard.Balance < 0)
                Assert.Fail();
        }

        [TestMethod]
        public void DeleteSavingCardTest()
        {
            InitializeAccountWithBudget(1000);
            InitializeSavingCards();
            account.DepositToSavingCard(savingCard, 500);
            InitializeBalancesBeforeTransaction();

            account.ReleaseFundsAndRemoveSavingCard(savingCard);

            Assert.AreEqual(account.Balance, balanceBeforeTransaction + 500);

            if (account.SavingCards.Contains(savingCard))
                Assert.Fail();
        }

        [TestMethod]
        public void TransferFundsBetweenSavingCardsTest()
        {
            InitializeAccountWithBudget(1000);
            InitializeSavingCards();
            account.DepositToSavingCard(savingCard, 200);

            IEnumerator<SavingCard> enumerator = account.SavingCards.GetEnumerator();
            enumerator.MoveNext();
            enumerator.MoveNext();
            SavingCard recipient = enumerator.Current;

            InitializeBalancesBeforeTransaction();
            decimal recipientBalanceBeforeTransaction = recipient.Balance;

            account.TransferFundsBetweenSavingCards(savingCard, recipient, 100);

            Assert.AreEqual(savingCardBalanceBeforeTransaction - 100, savingCard.Balance);
            Assert.AreEqual(recipient.Balance - 100, recipientBalanceBeforeTransaction);
        }

        [TestMethod]
        public void TransferTooManyFundsBetweenSavingCardsTest()
        {
            try
            {
                InitializeAccountWithBudget(1000);
                InitializeSavingCards();
                account.DepositToSavingCard(savingCard, 200);

                IEnumerator<SavingCard> enumerator = account.SavingCards.GetEnumerator();
                enumerator.MoveNext();
                enumerator.MoveNext();
                SavingCard recipient = enumerator.Current;

                InitializeBalancesBeforeTransaction();
                decimal recipientBalanceBeforeTransaction = recipient.Balance;

                account.TransferFundsBetweenSavingCards(savingCard, recipient, 300);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.GetType(), typeof(ArgumentException));
            }

            if (savingCard.Balance < 0)
                Assert.Fail();
        }

        [TestMethod]
        public void AddRegularIncomeToAccountTest()
        {
            InitializeAccount();

            RegularTransaction regularIncome = new RegularTransaction("FirstRegularIncome",
                                                            "This is what the company pays me.",
                                                            DateTime.Now,
                                                            4,
                                                            1500);
            account.RegularTransactions.Add(regularIncome);

            Assert.IsTrue(account.RegularTransactions.Contains(regularIncome));
        }

        [TestMethod]
        public void AddRegularExpenseToAccountTest()
        {
            InitializeAccount();

            RegularTransaction regularExpense = new RegularTransaction("FirstRegularIncome",
                                                            "This is what the company pays me.",
                                                            DateTime.Now,
                                                            4,
                                                            -1500);
            account.RegularTransactions.Add(regularExpense);

            Assert.IsTrue(account.RegularTransactions.Contains(regularExpense));
        }

        [TestMethod]
        public void TotalMoneyOnAccountTest()
        {
            InitializeAccount();
            InitializeSavingCards();

            account.Deposit(1000);
            account.DepositToSavingCard(savingCards[0], 500);
            account.DepositToSavingCard(savingCards[1], 300);

            Assert.AreEqual(account.TotalMoney, 1000);
        }

        [TestMethod]
        public void CollectRegularTransactionsTest()
        {
            InitializeAccount();
            InitializeRegularTransactions();

            account.CollectRegularTransactions();

            Assert.AreEqual(1700, account.Balance);
        }

        private void InitializeRegularTransactions()
        {
            regularTransactions.Add(new RegularTransaction("Plata",
                                                           "Redovna plata",
                                                           DateTime.Now,
                                                           DateTime.Now.Day,
                                                           1000));

            regularTransactions.Add(new RegularTransaction("Plata",
                                                           "Redovna plata",
                                                           DateTime.Now,
                                                           DateTime.Now.Day + 1,
                                                           600));

            regularTransactions.Add(new RegularTransaction("Plata",
                                                           "Redovna plata",
                                                           DateTime.Now.AddDays(-5),
                                                           DateTime.Now.Day - 1,
                                                           700));

            account.RegularTransactions = regularTransactions;
        }

        private void InitializeAccountWithBudget(decimal budget)
        {
            account = new Account("RandomAccount", budget, new List<SavingCard>(), new List<RegularTransaction>());
        }
        private void InitializeSavingCards()
        {
            SavingCard savingCard1;

            savingCard = new SavingCard("RandomSavingCard", "For saving purposes.", account);
            account.SavingCards.Add(savingCard);
            savingCard1 = new SavingCard("AnotherSavingCard", "For saving purposes.", account);
            account.SavingCards.Add(savingCard1);

            savingCards.Add(savingCard);
            savingCards.Add(savingCard1);
        }

        
        private void InitializeBalancesBeforeTransaction()
        {
            balanceBeforeTransaction = account.Balance;
            savingCardBalanceBeforeTransaction = savingCard.Balance;
        }
        private void InitializeStandardTestingConditions()
        {
            InitializeAccountWithBudget(9000);
            InitializeSavingCards();
            InitializeBalancesBeforeTransaction();
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using MoneyManagmentApp;
using MoneyManagmentApp.Models;
using Logging;

namespace UnitTestMoneyManagment
{
    [TestClass]
    public class TransactionLoggerTest
    {
        [TestMethod]
        public void CreateLoggerAndMakeInitialLog()
        {
            FileLogger transactionLogger = new FileLogger();

            double randomNumber = new Random().NextDouble();
            transactionLogger.Log(randomNumber.ToString());

            Assert.IsTrue(EntryExistsInLog(randomNumber.ToString()));
        }

        [TestMethod]
        public void LogDepositToAccountTest()
        {
            //Account account = new Account("Acc1", 9000, null, null);
        }

        private bool EntryExistsInLog(string entry)
        {
            string logPath = ConfigurationManager.AppSettings["LogFilePath"];

            string[] logContent = File.ReadAllLines(logPath);

            return logContent.Contains(entry);
        }
    }
}

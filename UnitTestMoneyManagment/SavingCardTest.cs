﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoneyManagmentApp.Models;

namespace UnitTestMoneyManagment
{
    [TestClass]
    public class SavingCardTest
    {
        private SavingCard savingCard;

        [TestMethod]
        public void CreateNewSavingCardTest()
        {
            InitializeSavingCard();

            Assert.AreEqual(savingCard.Name, "RandomSavingCard");
        }

        private void InitializeSavingCard()
        {
            savingCard = new SavingCard("RandomSavingCard", "For saving purposes", null);
        }

        
    }
}
